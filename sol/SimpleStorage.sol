pragma solidity >=0.4.0 <0.7.0;

contract SimpleStorage {
    uint storedData;

    function set(uint number) public {
        storedData = number;
    }

    function get() public view returns (uint data) {
        return storedData;
    }
}
