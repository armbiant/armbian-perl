unit class Eth::Minimalistic;

use Net::Ethereum;

has Str  $.apiurl;
has Int  $.ethacc = 0;
has Int  $.gasqty = 20000000;
has Int  $.gascap = 15; # percentage cap for eth_estimateGas
has Str  $.sctx   = ( "0x" ~ q{0} x 64 );
has Str  $.scaddr = ( "0x" ~ q{0} x 40 );
has Str  $.evsign = 'storage_access(uint8,bytes32)';
has Any  $.ethobj = Nil;

submethod BUILD(
    Str  :$abi,
    Str  :$apiurl,
    Str  :$sctx,
    Str  :$unlockpwd?,
) returns Bool {
    my Bool $rc = False;
    $!sctx      = $sctx;
    if ( $apiurl ) {
        $!ethobj     = Net::Ethereum.new(
             api_url   => $apiurl,
             abi       => $abi,
             keepalive => True,
             unlockpwd => $unlockpwd,
        );
        $!apiurl = $apiurl;
        $rc      = True;
    }
    $rc;
}

method !read_blockchain( Str :$method, Hash :$data? ) returns Cool {
    my Cool $rc = Nil;
    my Hash $dt = $data if ( $data && $data.pairs );
    if (
        $method &&
        $!ethobj &&
        !( $!scaddr ~~ m:i/^ 0x<[0]>**40 $/ )
    ) {
        $rc = $!ethobj.contract_method_call( $method, $dt );
    }
    $rc;
}

method !write_blockchain(
    Str  :$method,
    Hash :$data?,
    Bool :$waittx
) returns Hash {
    my %ret     = status => False, txhash => ( "0x" ~ q{0} x 64 );
    my Hash $dt = $data if ( $data && $data.pairs );
    if (
        $method &&
        $!ethobj &&
        !( $!scaddr ~~ m:i/^ 0x<[0]>**40 $/ )
    ) {
        my UInt $txgas = $!ethobj.contract_method_call_estimate_gas(
            $method,
            $dt,
        );
        $txgas += floor(
            ($txgas/100) * ( $txgas >= $!gasqty/5 ?? $!gascap !! $!gascap * 3 )
        );
        my Str $tx  = $!ethobj.sendTransaction(
            $!ethobj.eth_accounts[$!ethacc],
            $!scaddr,
            $method,
            $dt,
            $txgas // $!gasqty,
        );
        # ('used gas "' ~ $method ~ '" <' ~ ( $data<rowdata> // q{} ) ~ '>: ' ~ $txgas).say;
        # ('used gas = ' ~ ($txgas // $!gasqty)).say;
        if ( $tx ~~ m:i/^ 0x<[0..9a..f]>**64 $/ ) {
            # ( $method ~ ':'  ~ $waittx).say;
            my @res =
                 $!ethobj.wait_for_transaction(
                     :hashes( @( $tx ) )
                 ) if $waittx;
            %ret<status> = True if ( @res.elems == 1 || !$waittx );
            %ret<txhash> = $tx;
            # ( $method ~ ':' ~ %ret<txhash> ).say;
        }
    }
    %ret;
}

method read_integer( Str :$method, Str :$rcname, Hash :$data? ) returns Int {
    my %callhash;
    my Int $rv   = -1;
    if $data.defined {
        %callhash = self!read_blockchain( :method( $method ), :data( $data ) );
    }
    else {
        %callhash = self!read_blockchain( :method( $method )  );
    }

    if ( (%callhash) && (%callhash{$rcname}:exists) ) {
        $rv = %callhash{$rcname}.Int;
    }
    $rv;
}

method initialize returns Bool {
    my Bool $ret = True;
    if ( self.is_node_active && self.set_contract_addr ) {
        if ( !( $!scaddr ~~ m:i/^ 0x<[0]>**40 $/ ) ) {
            $!ethobj.contract_id = $!scaddr;
        } else {
            $ret = False;
        }
    }
    $ret;
}

method is_node_active returns Bool {
    my Bool $rc = False;
    if ( $!ethobj ) {
        my %h = $!ethobj.node_ping;
        if ( %h<retcode>:exists) {
            $rc = True if ( %h<retcode> == 0 );
        }
    }
    $rc;
}

method set_contract_addr returns Bool {
    my Bool $rc = False;
    if ( $!ethobj && !( $!sctx ~~ m:i/^ 0x<[0]>**64 $/ ) ) {
        $!scaddr = $!ethobj.retrieve_contract($!sctx);
        $rc = True;
    }
    $rc;
}

method get_data returns UInt {
    my UInt $ret = 0;
    $ret = self.read_integer(
        :method( 'get' ),
        :rcname( 'data' )
    );
    $ret;
}

method set_data( UInt :$number, Bool :$waittx ) returns Hash {
    self!write_blockchain(
        :method( 'set' ),
        :data( %( number => $number ) ),
        :waittx( $waittx // True ),
    );
}
