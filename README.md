# Presentation essentials for German Perl Workshop 2020

This is essentials for German Perl Workshop 2020 talk ["Querying the Ethereum blockchain nodes with Raku"](https://act.yapc.eu/gpw2020/talk/7696).

## Eth::Minimalistic

This is [minimalistic module](https://gitlab.com/pheix-research/gpw2020/-/blob/master/lib/Eth/Minimalistic.pm6) for ethereum node interactions. This module is not presented in Raku ecosystem, so you need to clone this repo and use it with `use lib <path>;`.

## Console application

Basic usage:

```bash
./consoleapp.p6 tx_hash [data_to_set]
```

- `tx_hash` - smart contract deploy transaction hash;

- `data_to_set` - data value to set to blockchain (optional).

Read data from blockchain:

```bash
./consoleapp.p6 0xcc7303b60add3a095762dcb9a0ee2e11376a765b8de5f10039e9e901c94d37df
```

Write data to block chain:

```bash
./consoleapp.p6 0xcc7303b60add3a095762dcb9a0ee2e11376a765b8de5f10039e9e901c94d37df 1982
```

## Web application

Basic usage:

```bash
./webapp.p6 tx_hash
```

- `tx_hash` - smart contract deploy transaction hash;

Example:

```bash
./webapp.p6 0xcc7303b60add3a095762dcb9a0ee2e11376a765b8de5f10039e9e901c94d37df
```

When application is started, you can access setter/getter HTML page at http://localhost:20001.

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
