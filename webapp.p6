#! /usr/bin/env perl6

use Cro::HTTP::Server;
use Cro::HTTP::Router;
use Net::Ethereum;

use lib './lib';
use Eth::Minimalistic;

die 'Usage: ./webapp.p6 tx_hash' unless @*ARGS[0].defined;

# Create blockchain object
my $eth = Eth::Minimalistic.new(
    abi       => './assets/solcoutput/SimpleStorage.abi'.IO.slurp,
    apiurl    => 'http://127.0.0.1:8540',
    sctx      => @*ARGS[0],
    unlockpwd => 'node0'
);

# Initialize ethereum object and die if init fails
die 'Ethereum node is down' unless $eth.initialize;

# Just form container
my $form = qq~
    <hr>
    <form method=POST action="/post">
        <input type="text" name="number" value="" required placeholder="1982">
        <input type="submit" value="Submit">
    </form>
~;

# Sample route
my $cmsapp = route {
    get -> {
        $eth.ethobj.personal_unlockAccount;
        content(
            'text/html',
            (
                '<p>actual data value: <a href="/">' ~ $eth.get_data ~
                '</a></p>' ~ $form
            )
        );
    }
    post -> 'post' {
        request-body -> $body {
            $eth.ethobj.personal_unlockAccount;
            my $status = $eth.set_data(
                number => $body.hash<number>.UInt,
                waittx => False
            );
            content(
                'text/html',
                (
                    '<p><b>data value <' ~ $body.hash<number> ~
                    '> is set via tx <' ~ $status<txhash> ~ '></b></p>' ~
                    '<p>actual data value: <a href="/">' ~ $eth.get_data ~
                    '</a></p>' ~ $form
                )
            );
        }
    }
}

# Create the HTTP service object
my Cro::Service $service = Cro::HTTP::Server.new(
    :host('localhost'), :port(20001), :application($cmsapp)
);

# Run it
$service.start;

# Cleanly shut down on Ctrl-C
react whenever signal(SIGINT) {
    $service.stop;
    exit;
}
